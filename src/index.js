import React from "react";
import ReactDOM from "react-dom";
import App from "./js/App";
import { createStore, compose } from "redux";
import { Provider } from "react-redux";

import reducer from "./js/redux/reducers";

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const store = createStore(reducer, composeEnhancers());
const store = createStore(reducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
