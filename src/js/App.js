import { Fragment, useState, useEffect } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import { useDispatch } from "react-redux";

import Header from "./components/Header";
import RecipeForm from "./components/RecipeForm";
import ListRecipes from "./components/ListRecipes";
import { updateRecipes } from "./redux/actions/recipeActions";

function App() {
  const [showFlag, setFlag] = useState(false);
  const dispatch = useDispatch();

  // update redux store with local db recipe data on mount.
  // update event listner to update redux store with local db recipe data if another session is updating local storage db.
  useEffect(() => {
    dispatch(updateRecipes());
    window.onstorage = () => dispatch(updateRecipes());
  }, [dispatch]);

  return (
    <Fragment>
      <CssBaseline />
      <Header addRecipeCB={() => setFlag(true)} homeCB={() => setFlag(false)} />
      {showFlag && <RecipeForm back={() => setFlag(false)} />}
      {!showFlag && <ListRecipes />}
    </Fragment>
  );
}

export default App;
