import { Fragment, useState } from "react";
import {
  Grid,
  TextField,
  InputLabel,
  Typography,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

function RecipeIngredients({ recipe, error, onChange, onDelete, ...props }) {
  const [rows, setRows] = useState(1);
  const classes = useStyles();

  const displayRow = (row) => {
    return (
      <Fragment key={row}>
        <Grid item xs={6} sm={4}>
          <TextField
            required
            id="name"
            name="name"
            value={
              recipe.ingredient &&
              recipe.ingredient[row - 1] &&
              recipe.ingredient[row - 1].name
                ? recipe.ingredient[row - 1].name
                : ""
            }
            onChange={(event) => onChange("name", row - 1, event.target.value)}
            error={
              error.ingredient &&
              error.ingredient[row - 1] &&
              error.ingredient[row - 1].name
                ? error.ingredient[row - 1].name
                : false
            }
            fullWidth
            variant="outlined"
          />
        </Grid>
        <Grid item xs={3} sm={4}>
          <TextField
            required
            type="number"
            id="quantity"
            name="quantity"
            value={
              recipe.ingredient &&
              recipe.ingredient[row - 1] &&
              recipe.ingredient[row - 1].quantity
                ? recipe.ingredient[row - 1].quantity
                : ""
            }
            onChange={(event) =>
              onChange("quantity", row - 1, event.target.value)
            }
            error={
              error.ingredient &&
              error.ingredient[row - 1] &&
              error.ingredient[row - 1].quantity
                ? error.ingredient[row - 1].quantity
                : false
            }
            fullWidth
            variant="outlined"
          />
        </Grid>
        <Grid item xs={3} sm={4}>
          <TextField
            required
            id="unit"
            name="unit"
            value={
              recipe.ingredient &&
              recipe.ingredient[row - 1] &&
              recipe.ingredient[row - 1].unit
                ? recipe.ingredient[row - 1].unit
                : ""
            }
            onChange={(event) => onChange("unit", row - 1, event.target.value)}
            error={
              error.ingredient &&
              error.ingredient[row - 1] &&
              error.ingredient[row - 1].unit
                ? error.ingredient[row - 1].unit
                : false
            }
            fullWidth
            variant="outlined"
          />
        </Grid>
      </Fragment>
    );
  };

  const getRow = () => {
    let cnt = 1;
    let jsx = [];
    while (cnt <= rows) {
      jsx.push(displayRow(cnt++));
    }

    return jsx;
  };
  return (
    <Fragment>
      <Typography component="h2" variant="h6" gutterBottom>
        Recipe Ingredients
      </Typography>
      <Grid container spacing={1}>
        <Grid item xs={6} sm={4}>
          <InputLabel>Name</InputLabel>
        </Grid>
        <Grid item xs={3} sm={4}>
          <InputLabel>Quantity</InputLabel>
        </Grid>
        <Grid item xs={3} sm={4}>
          <InputLabel>Unit</InputLabel>
        </Grid>
        {getRow()}
      </Grid>
      <div className={classes.buttons}>
        {rows > 1 && (
          <Button
            size="small"
            variant="contained"
            className={classes.button}
            onClick={() => {
              setRows((prev) => {
                onDelete(prev - 1);
                return prev - 1;
              });
            }}
          >
            Delete
          </Button>
        )}
        <Button
          size="small"
          variant="contained"
          className={classes.button}
          onClick={() => setRows((prev) => prev + 1)}
        >
          Add more
        </Button>
      </div>
    </Fragment>
  );
}

export default RecipeIngredients;
