import { Grid, TextField, InputLabel } from "@material-ui/core";

function RecipeDetails({ recipe, error, onChange }) {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <TextField
          required
          id="dishName"
          name="dishName"
          label="Recipe name"
          value={recipe.name ? recipe.name : ""}
          onChange={(value) => onChange("name", value)}
          error={error.name ? error.name : false}
          fullWidth
        />
      </Grid>
      <Grid item xs={12} sm={3}>
        <InputLabel>Upload recipe image</InputLabel>
      </Grid>
      <Grid item xs={12} sm={9}>
        <TextField
          id="dishImage"
          name="dishImage"
          type="file"
          accept="image/*"
          onChange={(value) => onChange("url", value)}
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          required
          variant="outlined"
          id="recipeSteps"
          name="recipeSteps"
          label="Recipe steps"
          multiline
          minRows={5}
          value={recipe.steps ? recipe.steps : ""}
          onChange={(value) => onChange("steps", value)}
          error={error.steps ? error.steps : false}
          fullWidth
        />
      </Grid>
    </Grid>
  );
}

export default RecipeDetails;
