import { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Paper,
  Typography,
  List,
  ListItem,
  ListItemText,
} from "@material-ui/core";
import { createSelectorCreator, defaultMemoize } from "reselect";
import { isEqual } from "lodash.isequal";
import { connect } from "react-redux";

import FilterBar from "./FilterBar";
/**
 * Creating a new selector to use deep comparision while checking for memoized values
 */
const createDeepEqualSelector = createSelectorCreator(defaultMemoize, isEqual);

const useStyles = makeStyles((theme) => ({
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
}));

function ListRecipes({ back, recipes, ...props }) {
  const classes = useStyles();

  return (
    <Fragment>
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center" gutterBottom>
            List of recipes from all users
          </Typography>
          {recipes.length > 0 && (
            <List component="ul">
              {recipes.map((recipe, index) => (
                <Fragment key={recipe.name}>
                  <ListItem button divider={true}>
                    <ListItemText primary={recipe.name} />
                  </ListItem>
                </Fragment>
              ))}
            </List>
          )}
          {recipes.length === 0 && (
            <Typography component="h1" variant="h6" align="center" gutterBottom>
              No recipes found
            </Typography>
          )}
        </Paper>
        <FilterBar />
      </main>
    </Fragment>
  );
}

const getFilterList = (state) => {
  // is string is blank then there is no filter
  // else split the comman seperated filter string
  if (!state.filter) {
    return false;
  } else {
    return state.filter.split(",");
  }
};
const getRecipes = (state) => state.recipes;

/**
 * Based on the filter ingredients, recipes are filtered before passed on to the components.
 * Also the results are memoized
 */
const getFilteredRecipes = createDeepEqualSelector(
  [getFilterList, getRecipes],
  (filterList, recipes) => {
    let filteredRecipes;
    if (filterList) {
      filteredRecipes = recipes.filter((recipe) => {
        const ingredientList = [];
        Object.values(recipe.ingredient).forEach((obj) => {
          ingredientList.push(obj.name.toLowerCase().trim());
        });
        const filterArray = filterList.map((filter) =>
          ingredientList.includes(filter.toLowerCase().trim())
        );
        return filterArray.some((result) => result);
      });
    } else {
      filteredRecipes = [...recipes];
    }
    return filteredRecipes;
  }
);

const mapStateToProps = (state) => {
  return {
    recipes: getFilteredRecipes(state),
  };
};

export default connect(mapStateToProps)(ListRecipes);
