import React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    position: "relative",
  },
  button: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    textTransform: "uppercase",
    marginLeft: theme.spacing(2),
  },
}));

export default function Header({ homeCB, addRecipeCB }) {
  const classes = useStyles();

  return (
    <AppBar position="absolute" className={classes.root} color="default">
      <Toolbar>
        <Button color="inherit" onClick={homeCB}>
          <Typography variant="body2" className={classes.title}>
            My Recipe App
          </Typography>
        </Button>
        <span style={{ flexGrow: 1 }}></span>
        <Button
          color="primary"
          className={classes.button}
          onClick={addRecipeCB}
        >
          <Typography variant="body2">Add New Recipe</Typography>
        </Button>
      </Toolbar>
    </AppBar>
  );
}
