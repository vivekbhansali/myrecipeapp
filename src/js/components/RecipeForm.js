import { Fragment, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Paper, Button, Typography } from "@material-ui/core";
import { useDispatch } from "react-redux";

import RecipeDetials from "./RecipeDetails";
import RecipeIngredients from "./RecipeIngredients";
import { addRecipe } from "../redux/actions/recipeActions";

const useStyles = makeStyles((theme) => ({
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  buttons: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

export default function RecipeForm({ back, ...props }) {
  const [recipe, setRecipe] = useState({});
  const [error, setError] = useState({});
  const dispatch = useDispatch();
  const classes = useStyles();

  /**
   * update recipe details fields i.e. recipe name, recipe image url, recipe steps.
   * @param {*} field
   * @param {*} event
   */
  const updateRecipeDetails = (field, event) => {
    if (field === "url") {
      setRecipe((prevState) => {
        const newState = { ...prevState };
        newState.url = URL.createObjectURL(event.target.files[0]);
        return newState;
      });
    } else {
      setRecipe((prevState) => {
        const newState = { ...prevState };
        newState[field] = event.target.value;
        return newState;
      });
      setError((prev) => {
        const newState = { ...prev };
        newState[field] = false;
        return newState;
      });
    }
  };

  /**
   * Update recipe ingredients details
   * @param {*} field
   * @param {*} index
   * @param {*} value
   */
  const updateIngredient = (field, index, value) => {
    setRecipe((prevState) => {
      const newState = { ...prevState };
      !newState.ingredient && (newState.ingredient = {});
      !newState.ingredient[index] &&
        (newState.ingredient[index] = { name: "", quantity: "", unit: "" });
      newState.ingredient[index][field] = value;
      return newState;
    });
    setError((prevState) => {
      const newState = { ...prevState };
      !newState.ingredient && (newState.ingredient = {});
      !newState.ingredient[index] &&
        (newState.ingredient[index] = {
          name: false,
          quantity: false,
          unit: false,
        });
      newState.ingredient[index][field] = false;
      return newState;
    });
  };

  /**
   * delete recipe ingredients
   * @param {*} index
   */
  const deleteIngredient = (index) => {
    setRecipe((prevState) => {
      const newState = { ...prevState };
      newState.ingredient &&
        newState.ingredient[index] &&
        delete newState.ingredient[index];
      return newState;
    });
    setError((prevState) => {
      const newState = { ...prevState };
      newState.ingredient &&
        newState.ingredient[index] &&
        delete newState.ingredient[index];
      return newState;
    });
  };

  /**
   * validate recipe form
   */
  const validateForm = () => {
    let name = false;
    let steps = false;
    let ingredientFlag = false;

    if (!recipe.name) {
      name = true;
    }
    if (!recipe.steps) {
      steps = true;
    }

    const initObj = { name: false, quantity: false, unit: false };
    let ingredient = {};
    // validate list of ingredients.
    if (!recipe.ingredient) {
      ingredient[0] = { name: true, quantity: true, unit: true };
    } else {
      Object.keys(recipe.ingredient).forEach((key) => {
        ingredient[key] = { ...initObj };
        Object.keys(recipe.ingredient[key]).forEach((field) => {
          !recipe.ingredient[key][field] && (ingredient[key][field] = true);
        });
      });
    }

    if (!name && !steps && !ingredientFlag) {
      dispatch(addRecipe(recipe));
      back();
    } else {
      setError({ name, steps, ingredient });
    }
  };

  return (
    <Fragment>
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center" gutterBottom>
            Add recipe details
          </Typography>
          <RecipeDetials
            recipe={recipe}
            error={error}
            onChange={updateRecipeDetails}
          />
          <RecipeIngredients
            recipe={recipe}
            error={error}
            onChange={updateIngredient}
            onDelete={deleteIngredient}
          />
          <div className={classes.buttons}>
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={validateForm}
            >
              Save recipe
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={back}
            >
              back
            </Button>
          </div>
        </Paper>
      </main>
    </Fragment>
  );
}
