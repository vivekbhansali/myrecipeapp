import React, { useState } from "react";
import {
  Paper,
  Grid,
  Typography,
  Button,
  TextField,
  makeStyles,
} from "@material-ui/core";
import { useDispatch, connect } from "react-redux";

import { updateFilter } from "../redux/actions/filterActions";
import { Fragment } from "react";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
}));

function FilterBar({ filterStr }) {
  const [filter, setFilter] = useState(filterStr);
  const dispatch = useDispatch();
  const classes = useStyles();

  return (
    <Fragment>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="subtitle2" gutterBottom>
          Filter the list of recipes based on the ingredients. Add more than one
          ingredients seperated by comma ",".
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={9} sm={9}>
            <TextField
              id="search"
              name="search"
              value={filter}
              onChange={(event) => setFilter(event.target.value)}
              fullWidth
              placeholder="Comma seperated ingredients"
            />
          </Grid>
          <Grid item xs={3} sm={3}>
            <Button
              size="small"
              variant="outlined"
              color="secondary"
              onClick={() => dispatch(updateFilter(filter))}
            >
              filter
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </Fragment>
  );
}

const mapStateToProps = (state) => {
  return {
    filterStr: state.filter,
  };
};

export default connect(mapStateToProps)(FilterBar);
