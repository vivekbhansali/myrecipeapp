import * as actions from "../constants/reciepActionTypes";

/**
 * add new recipe to local db as well as redux store
 * @param {*} recipe
 * @returns
 */
export const addRecipe = (recipe) => {
  let recipes = JSON.parse(localStorage.getItem("recipes"));
  recipes ? recipes.push(recipe) : (recipes = [recipe]);
  localStorage.setItem("recipes", JSON.stringify(recipes));
  return {
    type: actions.addRecipe,
    payload: recipe,
  };
};

// fetch recipe data from local db
// update redux store
export const updateRecipes = () => {
  let recipes = JSON.parse(localStorage.getItem("recipes"));
  !recipes && (recipes = []);
  return {
    type: actions.updateRecipes,
    payload: recipes,
  };
};
