import * as actions from "./../constants/filterActionTypes";

export const updateFilter = (filter) => ({
  type: actions.updateFilter,
  payload: filter,
});
