import * as actions from "../constants/reciepActionTypes";

const recipeReducer = (state = [], action) => {
  let newState = [...state];
  switch (action.type) {
    case actions.addRecipe:
      newState.push(action.payload);
      break;

    case actions.updateRecipes:
      newState = action.payload;
      break;
    default:
  }
  return newState;
};
export default recipeReducer;
