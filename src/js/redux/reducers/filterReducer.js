import * as actions from "../constants/filterActionTypes";

const filterReducers = (state = "", action) => {
  let newState = state;
  switch (action.type) {
    case actions.updateFilter:
      newState = action.payload;
      break;

    default:
  }
  return newState;
};
export default filterReducers;
