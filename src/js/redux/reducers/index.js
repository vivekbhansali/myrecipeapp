import { combineReducers } from "redux";
import recipeReducer from "./recipeReducer";
import filterReducers from "./filterReducer";

export default combineReducers({
  recipes: recipeReducer,
  filter: filterReducers,
});
